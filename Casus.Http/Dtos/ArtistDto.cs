﻿using System;

namespace Casus.Http.Dtos
{
    public class ArtistDto
    {
        public Guid ArtistGuid { get; set; }
        public string Name { get; set; }
    }
}