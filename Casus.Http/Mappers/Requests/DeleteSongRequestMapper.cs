﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Inputs;
using Casus.Http.Requests;

namespace Casus.Http.Mappers.Requests
{
    public class DeleteSongRequestMapper : IMapper<DeleteSongRequest, DeleteSongInput>
    {
        public DeleteSongInput Map(DeleteSongRequest from)
        {
            return new DeleteSongInput
            {
                Guid = from.Guid
            };
        }
    }
}