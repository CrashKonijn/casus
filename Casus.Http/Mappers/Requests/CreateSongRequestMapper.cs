﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Inputs;
using Casus.Http.Requests;

namespace Casus.Http.Mappers.Requests
{
    public class CreateSongRequestMapper : IMapper<CreateSongRequest, CreateSongInput>
    {
        public CreateSongInput Map(CreateSongRequest from)
        {
            return new CreateSongInput
            {
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album,
                ArtistGuid = from.ArtistGuid
            };
        }
    }
}