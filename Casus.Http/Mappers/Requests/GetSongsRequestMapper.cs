﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Inputs;
using Casus.Http.Requests;

namespace Casus.Http.Mappers.Requests
{
    public class GetSongsRequestMapper : IMapper<GetSongsRequest, GetSongsInput>
    {
        public GetSongsInput Map(GetSongsRequest @from)
        {
            return new GetSongsInput
            {
                Genre = from.Genre
            };
        }
    }
}