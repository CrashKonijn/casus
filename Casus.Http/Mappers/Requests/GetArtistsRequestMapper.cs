﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Inputs;
using Casus.Http.Requests;

namespace Casus.Http.Mappers.Requests
{
    public class GetArtistsRequestMapper : IMapper<GetArtistsRequest, GetArtistsInput>
    {
        public GetArtistsInput Map(GetArtistsRequest from)
        {
            return new GetArtistsInput();
        }
    }
}