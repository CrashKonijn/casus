﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Inputs;
using Casus.Http.Requests;

namespace Casus.Http.Mappers.Requests
{
    public class ImportDataRequestMapper : IMapper<ImportDataRequest, ImportDataInput>
    {
        public ImportDataInput Map(ImportDataRequest @from)
        {
            return new ImportDataInput();
        }
    }
}