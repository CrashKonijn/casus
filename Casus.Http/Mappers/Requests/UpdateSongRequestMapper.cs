﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Inputs;
using Casus.Http.Requests;

namespace Casus.Http.Mappers.Requests
{
    public class UpdateSongRequestMapper : IMapper<UpdateSongRequest, UpdateSongInput>
    {
        public UpdateSongInput Map(UpdateSongRequest from)
        {
            return new UpdateSongInput
            {
                SongGuid = from.SongGuid,
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album,
                ArtistGuid = from.ArtistGuid
            };
        }
    }
}