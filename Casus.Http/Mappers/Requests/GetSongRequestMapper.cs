﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Inputs;
using Casus.Http.Requests;

namespace Casus.Http.Mappers.Requests
{
    public class GetSongRequestMapper : IMapper<GetSongRequest, GetSongInput>
    {
        public GetSongInput Map(GetSongRequest from)
        {
            return new GetSongInput
            {
                Guid = from.Guid
            };
        }
    }
}