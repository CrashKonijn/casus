﻿using Casus.Domain.Interfaces;
using Casus.Domain.Models;
using Casus.Http.Dtos;

namespace Casus.Http.Mappers
{
    public class SongDtoMapper : IMapper<Song, SongDto>
    {
        public SongDto Map(Song @from)
        {
            return new SongDto
            {
                SongGuid = from.SongGuid,
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album,
                ArtistGuid = from.ArtistGuid
            };
        }
    }
}