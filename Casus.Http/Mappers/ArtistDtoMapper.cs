﻿using Casus.Domain.Interfaces;
using Casus.Domain.Models;
using Casus.Http.Dtos;

namespace Casus.Http.Mappers
{
    public class ArtistDtoMapper : IMapper<Artist, ArtistDto>
    {
        public ArtistDto Map(Artist from)
        {
            return new ArtistDto
            {
                ArtistGuid = from.ArtistGuid,
                Name = from.Name
            };
        }
    }
}