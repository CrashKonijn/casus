﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Outputs;
using Casus.Http.Responses;

namespace Casus.Http.Mappers.Responses
{
    public class GetSongResponseMapper : IMapper<GetSongOutput, GetSongResponse>
    {
        private readonly SongDtoMapper songMapper;

        public GetSongResponseMapper(SongDtoMapper songMapper)
        {
            this.songMapper = songMapper;
        }

        public GetSongResponse Map(GetSongOutput from)
        {
            return new GetSongResponse
            {
                Data = this.songMapper.Map(from.Song)
            };
        }
    }
}