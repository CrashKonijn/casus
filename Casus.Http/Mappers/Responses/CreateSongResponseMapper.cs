﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Outputs;
using Casus.Http.Responses;

namespace Casus.Http.Mappers.Responses
{
    public class CreateSongResponseMapper : IMapper<CreateSongOutput, CreateSongResponse>
    {
        private readonly SongDtoMapper songMapper;

        public CreateSongResponseMapper(SongDtoMapper songMapper)
        {
            this.songMapper = songMapper;
        }

        public CreateSongResponse Map(CreateSongOutput from)
        {
            return new CreateSongResponse
            {
                Data = this.songMapper.Map(from.Song)
            };
        }
    }
}