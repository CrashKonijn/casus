﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Outputs;
using Casus.Http.Responses;

namespace Casus.Http.Mappers.Responses
{
    public class DeleteSongResponseMapper : IMapper<DeleteSongOutput, DeleteSongResponse>
    {
        public DeleteSongResponse Map(DeleteSongOutput from)
        {
            return new DeleteSongResponse
            {
                Data = from.Guid
            };
        }
    }
}