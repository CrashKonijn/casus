﻿using System.Linq;
using Casus.Domain.Interfaces;
using Casus.Domain.Models;
using Casus.Domain.UseCases.Outputs;
using Casus.Http.Dtos;
using Casus.Http.Responses;

namespace Casus.Http.Mappers.Responses
{
    public class GetSongsResponseMapper : IMapper<GetSongsOutput, GetSongsResponse>
    {
        private readonly SongDtoMapper songMapper;

        public GetSongsResponseMapper(SongDtoMapper songMapper)
        {
            this.songMapper = songMapper;
        }

        public GetSongsResponse Map(GetSongsOutput from)
        {
            return new GetSongsResponse
            {
                Data = from.Songs.Select(this.songMapper.Map).ToList()
            };
        }
    }
}