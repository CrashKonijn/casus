﻿using System.Linq;
using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Outputs;
using Casus.Http.Responses;

namespace Casus.Http.Mappers.Responses
{
    public class GetArtistsResponseMapper : IMapper<GetArtistsOutput, GetArtistsResponse>
    {
        private readonly ArtistDtoMapper artistDtoMapper;

        public GetArtistsResponseMapper(ArtistDtoMapper artistDtoMapper)
        {
            this.artistDtoMapper = artistDtoMapper;
        }

        public GetArtistsResponse Map(GetArtistsOutput @from)
        {
            return new GetArtistsResponse
            {
                Data = from.Artists.Select(this.artistDtoMapper.Map).ToList()
            };
        }
    }
}