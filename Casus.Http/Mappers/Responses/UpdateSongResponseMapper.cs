﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Outputs;
using Casus.Http.Responses;

namespace Casus.Http.Mappers.Responses
{
    public class UpdateSongResponseMapper : IMapper<UpdateSongOutput, UpdateSongResponse>
    {
        private readonly SongDtoMapper songDtoMapper;

        public UpdateSongResponseMapper(SongDtoMapper songDtoMapper)
        {
            this.songDtoMapper = songDtoMapper;
        }

        public UpdateSongResponse Map(UpdateSongOutput from)
        {
            return new UpdateSongResponse
            {
                Data = this.songDtoMapper.Map(from.Song)
            };
        }
    }
}