﻿using Casus.Domain.Interfaces;
using Casus.Domain.UseCases.Outputs;
using Casus.Http.Responses;

namespace Casus.Http.Mappers.Responses
{
    public class ImportDataResponseMapper : IMapper<ImportDataOutput, ImportDataResponse>
    {
        public ImportDataResponse Map(ImportDataOutput from)
        {
            return new ImportDataResponse
            {
                ImportedArtistsCount = from.ImportedArtistsCount,
                ImportedSongsCount = from.ImportedSongsCount
            };
        }
    }
}