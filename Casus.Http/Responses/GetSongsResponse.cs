﻿using System.Collections.Generic;
using Casus.Http.Dtos;

namespace Casus.Http.Responses
{
    public class GetSongsResponse
    {
        public List<SongDto> Data { get; set; }
    }
}