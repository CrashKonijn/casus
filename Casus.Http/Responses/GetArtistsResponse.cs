﻿using System.Collections.Generic;
using Casus.Http.Dtos;

namespace Casus.Http.Responses
{
    public class GetArtistsResponse
    {
        public List<ArtistDto> Data { get; set; }
    }
}