﻿namespace Casus.Http.Responses
{
    public class ImportDataResponse
    {
        public int ImportedArtistsCount { get; set; }
        public int ImportedSongsCount { get; set; }
    }
}