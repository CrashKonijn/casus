﻿using Casus.Domain.Models;
using Casus.Http.Dtos;

namespace Casus.Http.Responses
{
    public class GetSongResponse
    {
        public SongDto Data { get; set; }
    }
}