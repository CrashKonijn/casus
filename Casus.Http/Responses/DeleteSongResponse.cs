﻿using System;

namespace Casus.Http.Responses
{
    public class DeleteSongResponse
    {
        public Guid Data { get; set; }
    }
}