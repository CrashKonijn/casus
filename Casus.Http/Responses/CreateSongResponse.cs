﻿using Casus.Http.Dtos;

namespace Casus.Http.Responses
{
    public class CreateSongResponse
    {
        public SongDto Data { get; set; }
    }
}