﻿using Casus.Http.Dtos;

namespace Casus.Http.Responses
{
    public class UpdateSongResponse
    {
        public SongDto Data { get; set; }
    }
}