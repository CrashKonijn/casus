﻿using System.Threading.Tasks;

namespace Casus.Http.Interfaces
{
    public interface IAction<TRequest, TResponse>
    {
        public Task<TResponse> Execute(TRequest request);
    }
}