﻿using System.Threading.Tasks;
using Casus.Domain.UseCases;
using Casus.Http.Interfaces;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Casus.Http.Requests;
using Casus.Http.Responses;

namespace Casus.Http.Actions
{
    public class ImportDataAction : IAction<ImportDataRequest, ImportDataResponse>
    {
        private readonly ImportDataUseCase useCase;
        private readonly ImportDataRequestMapper requestMapper;
        private readonly ImportDataResponseMapper responseMapper;

        public ImportDataAction(ImportDataUseCase useCase, ImportDataRequestMapper requestMapper, ImportDataResponseMapper responseMapper)
        {
            this.useCase = useCase;
            this.requestMapper = requestMapper;
            this.responseMapper = responseMapper;
        }

        public async Task<ImportDataResponse> Execute(ImportDataRequest request)
        {
            var output = await this.useCase.Execute(
                this.requestMapper.Map(request)
            );

            return this.responseMapper.Map(output);
        }
    }
}