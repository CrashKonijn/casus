﻿using System.Threading.Tasks;
using Casus.Domain.UseCases;
using Casus.Http.Interfaces;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Casus.Http.Requests;
using Casus.Http.Responses;

namespace Casus.Http.Actions
{
    public class GetSongAction : IAction<GetSongRequest, GetSongResponse>
    {
        private readonly GetSongUseCase useCase;
        private readonly GetSongRequestMapper requestMapper;
        private readonly GetSongResponseMapper responseMapper;

        public GetSongAction(GetSongUseCase useCase, GetSongRequestMapper requestMapper, GetSongResponseMapper responseMapper)
        {
            this.useCase = useCase;
            this.requestMapper = requestMapper;
            this.responseMapper = responseMapper;
        }

        public async Task<GetSongResponse> Execute(GetSongRequest request)
        {
            var output = await this.useCase.Execute(
                this.requestMapper.Map(request)
            );

            return this.responseMapper.Map(output);
        }
    }
}