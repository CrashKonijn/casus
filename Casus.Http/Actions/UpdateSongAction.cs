﻿using System.Threading.Tasks;
using Casus.Domain.UseCases;
using Casus.Http.Interfaces;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Casus.Http.Requests;
using Casus.Http.Responses;

namespace Casus.Http.Actions
{
    public class UpdateSongAction : IAction<UpdateSongRequest, UpdateSongResponse>
    {
        private readonly UpdateSongUseCase useCase;
        private readonly UpdateSongRequestMapper requestMapper;
        private readonly UpdateSongResponseMapper responseMapper;

        public UpdateSongAction(UpdateSongUseCase useCase, UpdateSongRequestMapper requestMapper, UpdateSongResponseMapper responseMapper)
        {
            this.useCase = useCase;
            this.requestMapper = requestMapper;
            this.responseMapper = responseMapper;
        }

        public async Task<UpdateSongResponse> Execute(UpdateSongRequest request)
        {
            var output = await this.useCase.Execute(
                this.requestMapper.Map(request)
            );

            return this.responseMapper.Map(output);
        }
    }
}