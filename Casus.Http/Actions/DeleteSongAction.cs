﻿using System.Threading.Tasks;
using Casus.Domain.UseCases;
using Casus.Http.Interfaces;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Casus.Http.Requests;
using Casus.Http.Responses;

namespace Casus.Http.Actions
{
    public class DeleteSongAction : IAction<DeleteSongRequest, DeleteSongResponse>
    {
        private readonly DeleteSongUseCase useCase;
        private readonly DeleteSongRequestMapper requestMapper;
        private readonly DeleteSongResponseMapper responseMapper;

        public DeleteSongAction(DeleteSongUseCase useCase, DeleteSongRequestMapper requestMapper, DeleteSongResponseMapper responseMapper)
        {
            this.useCase = useCase;
            this.requestMapper = requestMapper;
            this.responseMapper = responseMapper;
        }

        public async Task<DeleteSongResponse> Execute(DeleteSongRequest request)
        {
            var output = await this.useCase.Execute(
                this.requestMapper.Map(request)
            );

            return this.responseMapper.Map(output);
        }
    }
}