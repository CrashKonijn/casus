﻿using System.Threading.Tasks;
using Casus.Domain.UseCases;
using Casus.Http.Interfaces;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Casus.Http.Requests;
using Casus.Http.Responses;

namespace Casus.Http.Actions
{
    public class GetSongsAction : IAction<GetSongsRequest, GetSongsResponse>
    {
        private readonly GetSongsUseCase useCase;
        private readonly GetSongsRequestMapper requestMapper;
        private readonly GetSongsResponseMapper responseMapper;

        public GetSongsAction(GetSongsUseCase getSongsUseCase, GetSongsRequestMapper requestMapper, GetSongsResponseMapper responseMapper)
        {
            this.useCase = getSongsUseCase;
            this.requestMapper = requestMapper;
            this.responseMapper = responseMapper;
        }

        public async Task<GetSongsResponse> Execute(GetSongsRequest request)
        {
            var output = await this.useCase.Execute(
                this.requestMapper.Map(request)
            );

            return this.responseMapper.Map(output);
        }
    }
}