﻿using System.Threading.Tasks;
using Casus.Domain.UseCases;
using Casus.Http.Interfaces;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Casus.Http.Requests;
using Casus.Http.Responses;

namespace Casus.Http.Actions
{
    public class GetArtistsAction : IAction<GetArtistsRequest, GetArtistsResponse>
    {
        private readonly GetArtistsUseCase useCase;
        private readonly GetArtistsRequestMapper requestMapper;
        private readonly GetArtistsResponseMapper responseMapper;

        public GetArtistsAction(GetArtistsUseCase getArtistsUseCase, GetArtistsRequestMapper requestMapper, GetArtistsResponseMapper responseMapper)
        {
            this.useCase = getArtistsUseCase;
            this.requestMapper = requestMapper;
            this.responseMapper = responseMapper;
        }

        public async Task<GetArtistsResponse> Execute(GetArtistsRequest request)
        {
            var output = await this.useCase.Execute(
                this.requestMapper.Map(request)
            );

            return this.responseMapper.Map(output);
        }
    }
}