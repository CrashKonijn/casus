﻿using System.Threading.Tasks;
using Casus.Domain.UseCases;
using Casus.Http.Interfaces;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Casus.Http.Requests;
using Casus.Http.Responses;

namespace Casus.Http.Actions
{
    public class CreateSongAction : IAction<CreateSongRequest, CreateSongResponse>
    {
        private readonly CreateSongUseCase useCase;
        private readonly CreateSongRequestMapper requestMapper;
        private readonly CreateSongResponseMapper responseMapper;

        public CreateSongAction(CreateSongUseCase useCase, CreateSongRequestMapper requestMapper, CreateSongResponseMapper responseMapper)
        {
            this.useCase = useCase;
            this.requestMapper = requestMapper;
            this.responseMapper = responseMapper;
        }

        public async Task<CreateSongResponse> Execute(CreateSongRequest request)
        {
            var output = await this.useCase.Execute(
                this.requestMapper.Map(request)
            );

            return this.responseMapper.Map(output);
        }
    }
}