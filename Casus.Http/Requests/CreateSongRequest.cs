﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Casus.Http.Requests
{
    public class CreateSongRequest
    {
        [BindRequired]
        [MinLength(3)]
        [MaxLength(200)]
        [NotNull]
        public string Name { get; set; }
        
        [BindRequired]
        public int Year { get; set; }
        
        [BindRequired]
        [MinLength(3)]
        [MaxLength(20)]
        public string ShortName { get; set; }
        
        public int? Bpm { get; set; }
        
        [BindRequired]
        public int Duration { get; set; }
        
        [BindRequired]
        [MinLength(3)]
        [MaxLength(200)]
        public string Genre { get; set; }
        
        public string SpotifyId { get; set; }
        
        [BindRequired]
        public string Album { get; set; }
        
        // Relations
        [BindRequired]
        public Guid ArtistGuid { get; set; }
    }
}