﻿namespace Casus.Http.Requests
{
    public class GetSongsRequest
    {
        public string Genre { get; set; }
    }
}