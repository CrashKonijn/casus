﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Casus.Http.Requests
{
    public class DeleteSongRequest
    {
        [BindRequired]
        public Guid Guid { get; set; }
    }
}