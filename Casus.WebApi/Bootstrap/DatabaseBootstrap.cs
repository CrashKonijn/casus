﻿using Casus.Database;
using Casus.Database.Mappers;
using Casus.Database.Persisters;
using Casus.Database.Repositories;
using Casus.Database.Updaters;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Interfaces.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Casus.WebApi.Bootstrap
{
    public static class DatabaseBootstrap
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddDbContext<CasusDbContext>();
            
            // Mappers
            services.AddScoped<ArtistMapper>();
            services.AddScoped<SongMapper>();
            
            // Updaters
            services.AddScoped<SongUpdater>();

            // Repositories
            services.AddScoped<ISongRepository, SongRepository>();
            services.AddScoped<IArtistRepository, ArtistRepository>();
            
            // Persisters
            services.AddScoped<IArtistPersister, ArtistPersister>();
            services.AddScoped<ISongPersister, SongPersister>();
        }
    }
}