﻿using Casus.Http.Actions;
using Casus.Http.Mappers;
using Casus.Http.Mappers.Requests;
using Casus.Http.Mappers.Responses;
using Microsoft.Extensions.DependencyInjection;

namespace Casus.WebApi.Bootstrap
{
    public static class HttpBootstrap
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // Mappers
            services.AddScoped<SongDtoMapper>();
            services.AddScoped<ArtistDtoMapper>();
            
            // Mappers/Requests
            services.AddScoped<ImportDataRequestMapper>();
            services.AddScoped<GetSongRequestMapper>();
            services.AddScoped<GetSongsRequestMapper>();
            services.AddScoped<CreateSongRequestMapper>();
            services.AddScoped<DeleteSongRequestMapper>();
            services.AddScoped<UpdateSongRequestMapper>();
            services.AddScoped<GetArtistsRequestMapper>();
            
            // Mappers/Responses
            services.AddScoped<ImportDataResponseMapper>();
            services.AddScoped<GetSongResponseMapper>();
            services.AddScoped<GetSongsResponseMapper>();
            services.AddScoped<CreateSongResponseMapper>();
            services.AddScoped<DeleteSongResponseMapper>();
            services.AddScoped<UpdateSongResponseMapper>();
            services.AddScoped<GetArtistsResponseMapper>();

            // Actions
            services.AddScoped<ImportDataAction>();
            services.AddScoped<GetSongAction>();
            services.AddScoped<GetSongsAction>();
            services.AddScoped<CreateSongAction>();
            services.AddScoped<DeleteSongAction>();
            services.AddScoped<UpdateSongAction>();
            services.AddScoped<GetArtistsAction>();
        }
    }
}