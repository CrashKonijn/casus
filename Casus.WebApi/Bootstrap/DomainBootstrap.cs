﻿using Casus.Database.Mappers;
using Casus.Domain.Mappers;
using Casus.Domain.UseCases;
using Microsoft.Extensions.DependencyInjection;
using SongMapper = Casus.Domain.Mappers.SongMapper;

namespace Casus.WebApi.Bootstrap
{
    public static class DomainBootstrap
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // UseCases
            services.AddScoped<ImportDataUseCase>();
            services.AddScoped<GetSongUseCase>();
            services.AddScoped<GetSongsUseCase>();
            services.AddScoped<CreateSongUseCase>();
            services.AddScoped<DeleteSongUseCase>();
            services.AddScoped<UpdateSongUseCase>();
            services.AddScoped<GetArtistsUseCase>();
            
            // Mappers
            services.AddScoped<ArtistDataMapper>();
            services.AddScoped<SongDataMapper>();
            services.AddScoped<SongMapper>();
            services.AddScoped<ArtistMapper>();
        }
    }
}