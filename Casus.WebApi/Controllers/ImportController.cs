﻿using System.Threading.Tasks;
using Casus.Http.Actions;
using Casus.Http.Requests;
using Casus.Http.Responses;
using Microsoft.AspNetCore.Mvc;

namespace Casus.WebApi.Controllers
{
    [ApiController]
    public class ImportController : Controller
    {
        private readonly ImportDataAction importDataAction;

        public ImportController(ImportDataAction importDataAction)
        {
            this.importDataAction = importDataAction;
        }

        [HttpGet]
        [Route("/v1/ImportData")]
        [ProducesResponseType(typeof(ImportDataResponse), 200)]
        public async Task<JsonResult> Get([FromQuery] ImportDataRequest request)
        {
            return new JsonResult(
                await this.importDataAction.Execute(request)
            );
        }
    }
}