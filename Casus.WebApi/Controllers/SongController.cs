﻿using System;
using System.Threading.Tasks;
using Casus.Http.Actions;
using Casus.Http.Requests;
using Casus.Http.Responses;
using Microsoft.AspNetCore.Mvc;

namespace Casus.WebApi.Controllers
{
    [ApiController]
    public class SongController : Controller
    {
        private readonly CreateSongAction createSongAction;
        private readonly GetSongAction getSongAction;
        private readonly GetSongsAction getSongsAction;
        private readonly DeleteSongAction deleteSongAction;
        private readonly UpdateSongAction updateSongAction;

        public SongController(GetSongsAction getSongsAction, GetSongAction getSongAction, CreateSongAction createSongAction, DeleteSongAction deleteSongAction, UpdateSongAction updateSongAction)
        {
            this.getSongsAction = getSongsAction;
            this.getSongAction = getSongAction;
            this.createSongAction = createSongAction;
            this.deleteSongAction = deleteSongAction;
            this.updateSongAction = updateSongAction;
        }

        [HttpPost]
        [Route("/v1/CreateSong")]
        [ProducesResponseType(typeof(CreateSongResponse), 200)]
        public async Task<JsonResult> Post([FromBody] CreateSongRequest request)
        {
            return new JsonResult(
                await this.createSongAction.Execute(request)
            );
        }

        [HttpGet]
        [Route("/v1/GetSong")]
        [ProducesResponseType(typeof(GetSongResponse), 200)]
        public async Task<JsonResult> Get([FromQuery] GetSongRequest request)
        {
            return new JsonResult(
                await this.getSongAction.Execute(request)
            );
        }

        [HttpGet]
        [Route("/v1/GetSongs")]
        [ProducesResponseType(typeof(GetSongsResponse), 200)]
        public async Task<JsonResult> Get([FromQuery] GetSongsRequest request)
        {
            return new JsonResult(
                await this.getSongsAction.Execute(request)
            );
        }

        [HttpDelete]
        [Route("/v1/DeleteSong")]
        [ProducesResponseType(typeof(DeleteSongResponse), 200)]
        public async Task<JsonResult> Delete([FromQuery] DeleteSongRequest request)
        {
            return new JsonResult(
                await this.deleteSongAction.Execute(request)
            );
        }

        [HttpPatch]
        [Route("/v1/UpdateSong")]
        [ProducesResponseType(typeof(UpdateSongResponse), 200)]
        public async Task<JsonResult> Patch([FromBody] UpdateSongRequest request)
        {
            return new JsonResult(
                await this.updateSongAction.Execute(request)
            );
        }
    }
}