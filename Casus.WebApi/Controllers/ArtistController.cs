﻿using System.Threading.Tasks;
using Casus.Http.Actions;
using Casus.Http.Requests;
using Casus.Http.Responses;
using Microsoft.AspNetCore.Mvc;

namespace Casus.WebApi.Controllers
{
    [ApiController]
    public class ArtistController : Controller
    {
        private readonly GetArtistsAction getArtistsAction;

        public ArtistController(GetArtistsAction getArtistsAction)
        {
            this.getArtistsAction = getArtistsAction;
        }

        [HttpGet]
        [Route("/v1/GetArtists")]
        [ProducesResponseType(typeof(GetArtistsResponse), 200)]
        public async Task<JsonResult> Get([FromQuery] GetArtistsRequest request)
        {
            return new JsonResult(
                await this.getArtistsAction.Execute(request)
            );
        }
    }
}