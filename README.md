# About

This is a casus for a job application, made in 4 hours by Peter Klooster (peter_klooster@hotmail.com).

# Swagger
Swagger can be found at https://localhost:5001/swagger/index.html

Open API spec can be found at https://localhost:5001/swagger/v1/swagger.json. I suggest importing this into PostMan/Insomnia etc.


