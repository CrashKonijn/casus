﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Casus.Domain.UnitTests.Support
{
    public class TestMockFactory : ServiceCollection
    {
        private readonly Dictionary<Type, Mock> mocks = new Dictionary<Type, Mock>();

        public Mock<T> Create<T>() where T : class
        {
            if (typeof(T).IsInterface)
            {
                this.CreateInterface<T>();
                return this.Get<T>();
            }
            
            this.CreateClass<T>();
            return this.Get<T>();
        }

        private void CreateInterface<T>() where T : class
        {
            this.mocks.Add(typeof(T), new Mock<T>());
        }
        
        private void CreateClass<T>() where T : class
        {
            ConstructorInfo ctor = typeof(T).GetConstructors().SingleOrDefault();
            if(ctor == null)
                throw new Exception($"Type '{typeof(T).Name}' has no constructor.");

            var parameters = ctor.GetParameters()
                .Select(
                    parameter => this.mocks.ContainsKey(parameter.ParameterType)
                        ? this.mocks[parameter.ParameterType].Object
                        : null
                ).ToArray();
            
            this.mocks.Add(typeof(T), new Mock<T>(parameters));
        }
        
        public Mock<T> Get<T>() where T : class
        {
            if (this.mocks.ContainsKey(typeof(T)))
                return (Mock<T>)this.mocks[typeof(T)];

            return default;
        }
        
        public IServiceProvider Build()
        {
            foreach ((Type key, Mock value) in this.mocks)
                this.AddSingleton(key, value.Object);

            return this.BuildServiceProvider();
        }
    }
}