﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casus.Domain.Interfaces.Repositories;
using Casus.Domain.Models;
using Casus.Domain.UnitTests.Support;
using Casus.Domain.UseCases;
using Casus.Domain.UseCases.Inputs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Casus.Domain.UnitTests.UseCases
{
    [TestClass]
    public class GetSongsUseCaseUnitTests
    {
        private readonly TestMockFactory factory = new TestMockFactory();
        
        [TestInitialize]
        public void Setup()
        {
            this.factory.Create<ISongRepository>();
            this.factory.AddSingleton<GetSongsUseCase>();
        }

        [TestMethod]
        public async Task Execute_WithoutGenre_CallsGetAll()
        {
            // Arrange
            this.factory.Get<ISongRepository>()
                .Setup(x => x.GetAll())
                .ReturnsAsync(new List<Song>());

            var useCase = this.factory.Build().GetService<GetSongsUseCase>();

            // Act
            await useCase.Execute(new GetSongsInput
            {
                Genre = null
            });

            // Assert
            this.factory.Get<ISongRepository>()
                .Verify(x => x.GetAll(), Times.Once);
            
            this.factory.Get<ISongRepository>()
                .Verify(x => x.SearchByGenre(It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public async Task Execute_WithGenre_CallsSearchByGenre()
        {
            // Arrange
            this.factory.Get<ISongRepository>()
                .Setup(x => x.GetAll())
                .ReturnsAsync(new List<Song>());

            var useCase = this.factory.Build().GetService<GetSongsUseCase>();

            // Act
            await useCase.Execute(new GetSongsInput
            {
                Genre = "Metal"
            });

            // Assert
            this.factory.Get<ISongRepository>()
                .Verify(x => x.GetAll(), Times.Never);
            
            this.factory.Get<ISongRepository>()
                .Verify(x => x.SearchByGenre(It.IsAny<string>()), Times.Once);
        }
    }
}