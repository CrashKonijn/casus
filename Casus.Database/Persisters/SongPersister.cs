﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casus.Database.Mappers;
using Casus.Database.Updaters;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Casus.Database.Persisters
{
    public class SongPersister : ISongPersister
    {
        private readonly CasusDbContext context;
        private readonly SongMapper songMapper;
        private readonly SongUpdater songUpdater;

        public SongPersister(SongMapper songMapper, CasusDbContext context, SongUpdater songUpdater)
        {
            this.songMapper = songMapper;
            this.context = context;
            this.songUpdater = songUpdater;
        }

        public async Task<Song> Update(Song model)
        {
            var entity = await this.context.Songs.FirstOrDefaultAsync(x => x.SongGuid == model.SongGuid);

            this.songUpdater.Update(model, entity);
            this.context.Songs.Update(entity);
            
            await this.context.SaveChangesAsync();
            
            return this.songMapper.Map(entity);
        }

        public async Task<Song> Store(Song model)
        {
            var entity = this.songMapper.Map(model);
            
            await this.context.AddAsync(entity);
            await this.context.SaveChangesAsync();

            return this.songMapper.Map(entity);
        }

        public async Task<Song> Delete(Guid guid)
        {
            var song = await this.context.Songs.FirstAsync(x => x.SongGuid == guid);
            this.context.Songs.Remove(song);
            await this.context.SaveChangesAsync();

            return this.songMapper.Map(song);
        }

        public async Task<List<Song>> StoreMany(List<Song> models)
        {
            var entities = models.Select(x => this.songMapper.Map(x)).ToList();

            await this.context.AddRangeAsync(entities);
            await this.context.SaveChangesAsync();

            return entities.Select(x => this.songMapper.Map(x)).ToList();
        }
    }
}