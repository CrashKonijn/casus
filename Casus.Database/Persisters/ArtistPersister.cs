﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casus.Database.Mappers;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Models;

namespace Casus.Database.Persisters
{
    public class ArtistPersister : IArtistPersister
    {
        private readonly CasusDbContext context;
        private readonly ArtistMapper artistMapper;

        public ArtistPersister(ArtistMapper artistMapper, CasusDbContext context)
        {
            this.artistMapper = artistMapper;
            this.context = context;
        }

        public Task<Artist> Update(Artist model)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Artist> Store(Artist model)
        {
            var entity = this.artistMapper.Map(model);
            
            await this.context.AddAsync(entity);
            await this.context.SaveChangesAsync();

            return this.artistMapper.Map(entity);
        }

        public Task<Artist> Delete(Guid guid)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Artist>> StoreMany(List<Artist> models)
        {
            var entities = models.Select(x => this.artistMapper.Map(x)).ToList();

            await this.context.AddRangeAsync(entities);
            await this.context.SaveChangesAsync();

            return entities.Select(x => this.artistMapper.Map(x)).ToList();
        }
    }
}