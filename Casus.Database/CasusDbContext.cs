﻿using System;
using System.IO;
using Casus.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Casus.Database
{
    public class CasusDbContext : DbContext
    {
        public DbSet<ArtistEntity> Artists { get; set; }
        public DbSet<SongEntity> Songs { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var dataSource = Path.Combine(Environment.CurrentDirectory, "..\\Casus.Database\\casus_database.db");
            options.UseSqlite($"Data Source={dataSource}");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArtistEntity>()
                .HasKey(a => a.ArtistGuid);
            
            modelBuilder.Entity<SongEntity>()
                .HasKey(s => s.SongGuid);
            
            modelBuilder.Entity<SongEntity>()
                .HasOne(s => s.Artist)
                .WithMany(a => a.Songs)
                .HasForeignKey(s => s.ArtistGuid);
        }
    }
}