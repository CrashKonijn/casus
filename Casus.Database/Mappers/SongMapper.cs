﻿using Casus.Database.Entities;
using Casus.Domain.Interfaces;
using Casus.Domain.Models;

namespace Casus.Database.Mappers
{
    public class SongMapper : IMapper<Song, SongEntity>, IMapper<SongEntity, Song>
    {
        public SongEntity Map(Song from)
        {
            return new SongEntity
            {
                SongGuid = from.SongGuid,
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album,
                ArtistGuid = from.ArtistGuid
            };
        }

        public Song Map(SongEntity from)
        {
            return new Song
            {
                SongGuid = from.SongGuid,
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album,
                ArtistGuid = from.ArtistGuid
            };
        }
    }
}