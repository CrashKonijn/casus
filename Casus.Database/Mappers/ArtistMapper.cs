﻿using Casus.Database.Entities;
using Casus.Domain.Interfaces;
using Casus.Domain.Models;

namespace Casus.Database.Mappers
{
    public class ArtistMapper : IMapper<Artist, ArtistEntity>, IMapper<ArtistEntity, Artist>
    {
        public ArtistEntity Map(Artist from)
        {
            return new ArtistEntity
            {
                ArtistGuid = from.ArtistGuid,
                Name = from.Name
            };
        }

        public Artist Map(ArtistEntity from)
        {
            return new Artist
            {
                ArtistGuid = from.ArtistGuid,
                Name = from.Name
            };
        }
    }
}