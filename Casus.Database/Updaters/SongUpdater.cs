﻿using Casus.Database.Entities;
using Casus.Domain.Interfaces;
using Casus.Domain.Models;

namespace Casus.Database.Updaters
{
    public class SongUpdater : IUpdater<Song, SongEntity>
    {
        public void Update(Song source, SongEntity target)
        {
            target.Name = source.Name;
            target.Year = source.Year;
            target.ShortName = source.ShortName;
            target.Bpm = source.Bpm;
            target.Duration = source.Duration;
            target.Genre = source.Genre;
            target.SpotifyId = source.SpotifyId;
            target.Album = source.Album;
            target.ArtistGuid = source.ArtistGuid;
        }
    }
}