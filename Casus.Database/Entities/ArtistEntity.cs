﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casus.Database.Entities
{
    [Table("Artists")]
    public class ArtistEntity
    {
        public Guid ArtistGuid { get; set; }
        public string Name { get; set; }
        
        // Relations
        public IEnumerable<SongEntity> Songs { get; set; }
    }
}