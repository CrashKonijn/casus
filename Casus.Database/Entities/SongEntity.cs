﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casus.Database.Entities
{
    [Table("Songs")]
    public class SongEntity
    {
        public Guid SongGuid { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string ShortName { get; set; }
        public int? Bpm { get; set; }
        public int Duration { get; set; }
        public string Genre { get; set; }
        public string SpotifyId { get; set; }
        public string Album { get; set; }
        
        // Relations
        public Guid ArtistGuid { get; set; }
        public ArtistEntity Artist { get; set; }
    }
}