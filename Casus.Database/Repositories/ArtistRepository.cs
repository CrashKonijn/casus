﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casus.Database.Mappers;
using Casus.Domain.Interfaces.Repositories;
using Casus.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Casus.Database.Repositories
{
    public class ArtistRepository : IArtistRepository
    {
        private readonly CasusDbContext context;
        private readonly ArtistMapper artistMapper;

        public ArtistRepository(CasusDbContext context, ArtistMapper artistMapper)
        {
            this.context = context;
            this.artistMapper = artistMapper;
        }

        public async Task<List<Artist>> GetAll()
        {
            var entities = await this.context.Artists.ToListAsync();

            return entities.Select(x => this.artistMapper.Map(x)).ToList();
        }

        public Task<Artist> Get(Guid guid)
        {
            throw new NotImplementedException();
        }
    }
}