﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casus.Database.Mappers;
using Casus.Domain.Interfaces.Repositories;
using Casus.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Casus.Database.Repositories
{
    public class SongRepository : ISongRepository
    {
        private readonly CasusDbContext context;
        private readonly SongMapper songMapper;

        public SongRepository(CasusDbContext context, SongMapper songMapper)
        {
            this.context = context;
            this.songMapper = songMapper;
        }

        public async Task<List<Song>> GetAll()
        {
            var entities = await this.context.Songs.ToListAsync();

            return entities.Select(x => this.songMapper.Map(x)).ToList();
        }

        public async Task<Song> Get(Guid guid)
        {
            var entity = await this.context.Songs.FirstOrDefaultAsync(c => c.SongGuid == guid);

            return this.songMapper.Map(entity);
        }

        public async Task<List<Song>> SearchByGenre(string genre)
        {
            var entities = await this.context.Songs.Where(x => x.Genre == genre).ToListAsync();

            return entities.Select(x => this.songMapper.Map(x)).ToList();
        }
    }
}