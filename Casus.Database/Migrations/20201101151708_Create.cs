﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Casus.Database.Migrations
{
    public partial class Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Artists",
                columns: table => new
                {
                    ArtistGuid = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artists", x => x.ArtistGuid);
                });

            migrationBuilder.CreateTable(
                name: "Songs",
                columns: table => new
                {
                    SongGuid = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Year = table.Column<int>(nullable: false),
                    ShortName = table.Column<string>(nullable: true),
                    Bpm = table.Column<int>(nullable: true),
                    Duration = table.Column<int>(nullable: false),
                    Genre = table.Column<string>(nullable: true),
                    SpotifyId = table.Column<string>(nullable: true),
                    Album = table.Column<string>(nullable: true),
                    ArtistGuid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Songs", x => x.SongGuid);
                    table.ForeignKey(
                        name: "FK_Songs_Artists_ArtistGuid",
                        column: x => x.ArtistGuid,
                        principalTable: "Artists",
                        principalColumn: "ArtistGuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Songs_ArtistGuid",
                table: "Songs",
                column: "ArtistGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Songs");

            migrationBuilder.DropTable(
                name: "Artists");
        }
    }
}
