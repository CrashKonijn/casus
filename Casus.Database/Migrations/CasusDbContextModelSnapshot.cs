﻿// <auto-generated />
using System;
using Casus.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Casus.Database.Migrations
{
    [DbContext(typeof(CasusDbContext))]
    partial class CasusDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.9");

            modelBuilder.Entity("Casus.Database.Entities.ArtistEntity", b =>
                {
                    b.Property<Guid>("ArtistGuid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("ArtistGuid");

                    b.ToTable("Artists");
                });

            modelBuilder.Entity("Casus.Database.Entities.SongEntity", b =>
                {
                    b.Property<Guid>("SongGuid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Album")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("ArtistGuid")
                        .HasColumnType("TEXT");

                    b.Property<int?>("Bpm")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Duration")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Genre")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("ShortName")
                        .HasColumnType("TEXT");

                    b.Property<string>("SpotifyId")
                        .HasColumnType("TEXT");

                    b.Property<int>("Year")
                        .HasColumnType("INTEGER");

                    b.HasKey("SongGuid");

                    b.HasIndex("ArtistGuid");

                    b.ToTable("Songs");
                });

            modelBuilder.Entity("Casus.Database.Entities.SongEntity", b =>
                {
                    b.HasOne("Casus.Database.Entities.ArtistEntity", "Artist")
                        .WithMany("Songs")
                        .HasForeignKey("ArtistGuid")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
