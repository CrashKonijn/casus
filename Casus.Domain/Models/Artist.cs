﻿using System;

namespace Casus.Domain.Models
{
    public class Artist
    {
        public Guid ArtistGuid { get; set; }
        public string Name { get; set; }
    }
}