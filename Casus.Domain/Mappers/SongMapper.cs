﻿using Casus.Domain.Interfaces;
using Casus.Domain.Models;
using Casus.Domain.UseCases.Inputs;

namespace Casus.Domain.Mappers
{
    public class SongMapper : IMapper<CreateSongInput, Song>, IMapper<UpdateSongInput, Song>
    {
        public Song Map(CreateSongInput from)
        {
            return new Song
            {
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album,
                ArtistGuid = from.ArtistGuid
            };
        }

        public Song Map(UpdateSongInput from)
        {
            return new Song
            {
                SongGuid = from.SongGuid,
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album,
                ArtistGuid = from.ArtistGuid
            };
        }
    }
}