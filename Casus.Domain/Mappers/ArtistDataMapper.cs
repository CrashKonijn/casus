﻿using Casus.Domain.Interfaces;
using Casus.Domain.Models;
using Casus.Domain.UseCases.DataModels;

namespace Casus.Domain.Mappers
{
    public class ArtistDataMapper : IMapper<ArtistData, Artist>
    {
        public Artist Map(ArtistData from)
        {
            return new Artist
            {
                Name = from.Name
            };
        }
    }
}