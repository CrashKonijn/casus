﻿using Casus.Domain.Interfaces;
using Casus.Domain.Models;
using Casus.Domain.UseCases.DataModels;

namespace Casus.Domain.Mappers
{
    public class SongDataMapper : IMapper<SongData, Song>
    {
        public Song Map(SongData from)
        {
            return new Song
            {
                Name = from.Name,
                Year = from.Year,
                ShortName = from.ShortName,
                Bpm = from.Bpm,
                Duration = from.Duration,
                Genre = from.Genre,
                SpotifyId = from.SpotifyId,
                Album = from.Album
            };
        }
    }
}