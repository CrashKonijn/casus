﻿using System.Threading.Tasks;
using Casus.Domain.Interfaces;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;

namespace Casus.Domain.UseCases
{
    public class DeleteSongUseCase : IUseCase<DeleteSongInput, DeleteSongOutput>
    {
        private readonly ISongPersister songPersister;

        public DeleteSongUseCase(ISongPersister songPersister)
        {
            this.songPersister = songPersister;
        }

        public async Task<DeleteSongOutput> Execute(DeleteSongInput input)
        {
            return new DeleteSongOutput
            {
                Guid = (await this.songPersister.Delete(input.Guid)).SongGuid
            };
        }
    }
}