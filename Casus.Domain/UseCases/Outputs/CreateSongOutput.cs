﻿using Casus.Domain.Models;

namespace Casus.Domain.UseCases.Outputs
{
    public class CreateSongOutput
    {
        public Song Song { get; set; }
    }
}