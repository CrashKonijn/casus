﻿using System;

namespace Casus.Domain.UseCases.Outputs
{
    public class DeleteSongOutput
    {
        public Guid Guid { get; set; }
    }
}