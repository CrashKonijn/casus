﻿using Casus.Domain.Models;

namespace Casus.Domain.UseCases.Outputs
{
    public class UpdateSongOutput
    {
        public Song Song { get; set; }
    }
}