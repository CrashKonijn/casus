﻿namespace Casus.Domain.UseCases.Outputs
{
    public class ImportDataOutput
    {
        public int ImportedArtistsCount { get; set; }
        public int ImportedSongsCount { get; set; }
    }
}