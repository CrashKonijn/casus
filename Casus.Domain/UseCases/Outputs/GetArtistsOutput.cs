﻿using System.Collections.Generic;
using Casus.Domain.Models;

namespace Casus.Domain.UseCases.Outputs
{
    public class GetArtistsOutput
    {
        public List<Artist> Artists { get; set; }
    }
}