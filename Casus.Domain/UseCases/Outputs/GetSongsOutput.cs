﻿using System.Collections.Generic;
using Casus.Domain.Models;

namespace Casus.Domain.UseCases.Outputs
{
    public class GetSongsOutput
    {
        public List<Song> Songs { get; set; }
    }
}