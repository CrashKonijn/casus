﻿using Casus.Domain.Models;

namespace Casus.Domain.UseCases.Outputs
{
    public class GetSongOutput
    {
        public Song Song { get; set; }
    }
}