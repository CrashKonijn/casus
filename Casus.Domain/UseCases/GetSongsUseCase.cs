﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casus.Domain.Interfaces;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Interfaces.Repositories;
using Casus.Domain.Models;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;

namespace Casus.Domain.UseCases
{
    public class GetSongsUseCase : IUseCase<GetSongsInput, GetSongsOutput>
    {
        private readonly ISongRepository songRepository;

        public GetSongsUseCase(ISongRepository songRepository)
        {
            this.songRepository = songRepository;
        }

        public async Task<GetSongsOutput> Execute(GetSongsInput input)
        {
            return new GetSongsOutput
            {
                Songs = await this.GetSongs(input)
            };
        }

        private Task<List<Song>> GetSongs(GetSongsInput input)
        {
            return input.Genre != null
                ? this.songRepository.SearchByGenre(input.Genre)
                : this.songRepository.GetAll();
        }
    }
}