﻿using System.Threading.Tasks;
using Casus.Domain.Interfaces;
using Casus.Domain.Interfaces.Repositories;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;

namespace Casus.Domain.UseCases
{
    public class GetSongUseCase : IUseCase<GetSongInput, GetSongOutput>
    {
        private readonly ISongRepository songRepository;

        public GetSongUseCase(ISongRepository songRepository)
        {
            this.songRepository = songRepository;
        }

        public async Task<GetSongOutput> Execute(GetSongInput input)
        {
            return new GetSongOutput
            {
                Song = await this.songRepository.Get(input.Guid)
            };
        }
    }
}