﻿namespace Casus.Domain.UseCases.DataModels
{
    public class SongData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Artist { get; set; }
        public string ShortName { get; set; }
        public int? Bpm { get; set; }
        public int Duration { get; set; }
        public string Genre { get; set; }
        public string SpotifyId { get; set; }
        public string Album { get; set; }
    }
}