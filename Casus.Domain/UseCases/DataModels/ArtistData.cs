﻿namespace Casus.Domain.UseCases.DataModels
{
    public class ArtistData
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}