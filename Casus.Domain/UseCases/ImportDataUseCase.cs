﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Casus.Domain.Interfaces;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Mappers;
using Casus.Domain.Models;
using Casus.Domain.UseCases.DataModels;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;
using Newtonsoft.Json.Linq;

namespace Casus.Domain.UseCases
{
    public class ImportDataUseCase : IUseCase<ImportDataInput, ImportDataOutput>
    {
        private readonly ArtistDataMapper artistDataMapper;
        private readonly SongDataMapper songDataMapper;
        private readonly IArtistPersister artistPersister;
        private readonly ISongPersister songPersister;

        public ImportDataUseCase(ArtistDataMapper artistDataMapper, SongDataMapper songDataMapper, IArtistPersister artistPersister, ISongPersister songPersister)
        {
            this.artistDataMapper = artistDataMapper;
            this.songDataMapper = songDataMapper;
            this.artistPersister = artistPersister;
            this.songPersister = songPersister;
        }

        public async Task<ImportDataOutput> Execute(ImportDataInput input)
        {
            var songs = this.LoadSongsData();
            
            var filteredSongs = songs
                .Where(x => x.Genre.Contains("Metal"))
                .Where(x => x.Year < 2016)
                .ToList();
            
            var artistNames = filteredSongs.Select(x => x.Artist).Distinct().ToList();
            
            var artists = this.LoadArtistData();
            var filteredArtists = artists.Where(x => artistNames.Contains(x.Name)).ToList();

            var artistModels = await this.StoreArtists(filteredArtists);
            var songModels = await this.StoreSongs(filteredSongs, artistModels);

            return new ImportDataOutput
            {
                ImportedArtistsCount = artistModels.Count,
                ImportedSongsCount = songModels.Count
            };
        }

        private List<ArtistData> LoadArtistData()
        {
            return this.LoadData<ArtistData>("artists.json");
        }

        private List<SongData> LoadSongsData()
        {
            return this.LoadData<SongData>("songs.json");
        }

        private List<T> LoadData<T>(string fileName)
        {
            var file = Path.Combine(Environment.CurrentDirectory, "wwwroot\\" + fileName);
            
            return JArray.Parse(
                File.ReadAllText(file)
            ).Select(x => x.ToObject<T>()).ToList();
        }

        private async Task<List<Artist>> StoreArtists(List<ArtistData> artists)
        {
            return await this.artistPersister.StoreMany(
                artists.Select(x => this.artistDataMapper.Map(x)).ToList()    
            );
        }

        private async Task<List<Song>> StoreSongs(List<SongData> songs, List<Artist> artists)
        {
            var songModels = songs.Select(songData =>
            {
                var song = this.songDataMapper.Map(songData);
                var guid = artists.Find(x => x.Name == songData.Artist)?.ArtistGuid;

                if (guid != null)
                    song.ArtistGuid = (Guid) guid;
                    
                return song;
            }).Where(x => x.ArtistGuid != Guid.Empty).ToList();
            
            return await this.songPersister.StoreMany(songModels);
        }
    }
}