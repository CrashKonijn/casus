﻿using System.Threading.Tasks;
using Casus.Domain.Interfaces;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Mappers;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;

namespace Casus.Domain.UseCases
{
    public class CreateSongUseCase : IUseCase<CreateSongInput, CreateSongOutput>
    {
        private readonly ISongPersister songPersister;
        private readonly SongMapper songMapper;

        public CreateSongUseCase(ISongPersister songPersister, SongMapper songMapper)
        {
            this.songPersister = songPersister;
            this.songMapper = songMapper;
        }

        public async Task<CreateSongOutput> Execute(CreateSongInput input)
        {
            var result = await this.songPersister.Store(
                this.songMapper.Map(input)
            );
            
            return new CreateSongOutput
            {
                Song = result
            };
        }
    }
}