﻿using System;

namespace Casus.Domain.UseCases.Inputs
{
    public class DeleteSongInput
    {
        public Guid Guid { get; set; }
    }
}