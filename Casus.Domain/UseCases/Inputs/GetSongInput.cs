﻿using System;

namespace Casus.Domain.UseCases.Inputs
{
    public class GetSongInput
    {
        public Guid Guid { get; set; }
    }
}