﻿namespace Casus.Domain.UseCases.Inputs
{
    public class GetSongsInput
    {
        public string Genre { get; set; }
    }
}