﻿using System.Threading.Tasks;
using Casus.Domain.Interfaces;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Mappers;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;

namespace Casus.Domain.UseCases
{
    public class UpdateSongUseCase : IUseCase<UpdateSongInput, UpdateSongOutput>
    {
        private readonly ISongPersister songPersister;
        private readonly SongMapper songMapper;

        public UpdateSongUseCase(ISongPersister songPersister, SongMapper songMapper)
        {
            this.songPersister = songPersister;
            this.songMapper = songMapper;
        }

        public async Task<UpdateSongOutput> Execute(UpdateSongInput input)
        {
            return new UpdateSongOutput
            {
                Song = await this.songPersister.Update(this.songMapper.Map(input))
            };
        }
    }
}