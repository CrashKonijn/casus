﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casus.Domain.Interfaces;
using Casus.Domain.Interfaces.Persisters;
using Casus.Domain.Interfaces.Repositories;
using Casus.Domain.Models;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;

namespace Casus.Domain.UseCases
{
    public class GetArtistsUseCase : IUseCase<GetArtistsInput, GetArtistsOutput>
    {
        private readonly IArtistRepository artistRepository;

        public GetArtistsUseCase(IArtistRepository artistRepository)
        {
            this.artistRepository = artistRepository;
        }

        public async Task<GetArtistsOutput> Execute(GetArtistsInput input)
        {
            return new GetArtistsOutput
            {
                Artists = await this.artistRepository.GetAll()
            };
        }
    }
}