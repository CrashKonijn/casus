﻿namespace Casus.Domain.Interfaces
{
    public interface IUpdater<TSource, TTarget>
    {
        public void Update(TSource source, TTarget target);
    }
}