﻿using System.Threading.Tasks;
using Casus.Domain.UseCases.Inputs;
using Casus.Domain.UseCases.Outputs;

namespace Casus.Domain.Interfaces
{
    public interface IUseCase<TInput, TOutput>
    {
        public Task<TOutput> Execute(TInput input);
    }
}