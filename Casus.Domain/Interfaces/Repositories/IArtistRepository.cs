﻿using Casus.Domain.Models;

namespace Casus.Domain.Interfaces.Repositories
{
    public interface IArtistRepository : ICrudRepository<Artist>
    {
        
    }
}