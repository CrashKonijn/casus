﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Casus.Domain.Interfaces.Repositories
{
    public interface ICrudRepository<TModel>
    {
        Task<List<TModel>> GetAll();

        Task<TModel> Get(Guid guid);
    }
}