﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casus.Domain.Models;

namespace Casus.Domain.Interfaces.Repositories
{
    public interface ISongRepository : ICrudRepository<Song>
    {
        Task<List<Song>> SearchByGenre(string genre);
    }
}