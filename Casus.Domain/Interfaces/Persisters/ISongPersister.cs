﻿using System;
using Casus.Domain.Models;

namespace Casus.Domain.Interfaces.Persisters
{
    public interface ISongPersister : ICrudPerstister<Song>
    {
        
    }
}