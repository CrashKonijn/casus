﻿using Casus.Domain.Models;

namespace Casus.Domain.Interfaces.Persisters
{
    public interface IArtistPersister : ICrudPerstister<Artist>
    {
        
    }
}