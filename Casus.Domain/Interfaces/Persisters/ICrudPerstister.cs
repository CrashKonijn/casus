﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Casus.Domain.Interfaces.Persisters
{
    public interface ICrudPerstister<TModel>
    {
        Task<TModel> Update(TModel model);
        
        Task<TModel> Store(TModel model);
        
        Task<TModel> Delete(Guid guid);

        Task<List<TModel>> StoreMany(List<TModel> models);
    }
}